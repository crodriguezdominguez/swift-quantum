//
//  QuantumComputing.h
//  QuantumComputing
//
//  Created by Carlos Rodríguez Domínguez on 30/6/16.
//  Copyright © 2016 Everyware Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for QuantumComputing.
FOUNDATION_EXPORT double QuantumComputingVersionNumber;

//! Project version string for QuantumComputing.
FOUNDATION_EXPORT const unsigned char QuantumComputingVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QuantumComputing/PublicHeader.h>


